﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab_Application
{
    public partial class OnlineGrocery : Form
    {       
        ArrayList arrayList = new ArrayList();
        ArrayList arrayListCart = new ArrayList();     
        InformationWindow informationwindow = new InformationWindow();
        int selected = -1;
        int selectedCart = -1;   
               
        public OnlineGrocery()
        {
            InitializeComponent();
        }

        private void OnlineGrocery_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);            
        }

        private void Addbtn_Click(object sender, EventArgs e)
        {
            string myPath;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Select Image.";
            openFileDialog.Filter = ".JPG|*.jpg|PNG|*.png";
            if (textBoxName.Text == String.Empty || textBoxPrice.Text == String.Empty || textBoxDes.Text == String.Empty || textBoxAmount.Text == String.Empty)
            {
                MessageBox.Show("Please don't leave textboxes empty!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);               
            }
            else
            {
                if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    pictureBox1.Image = new Bitmap(openFileDialog.FileName);
                    myPath = System.IO.Path.GetFullPath(openFileDialog.FileName);

                    Products tmpProducts = new Products(textBoxName.Text, textBoxPrice.Text, textBoxDes.Text, myPath, textBoxAmount.Text);
                    arrayList.Add(tmpProducts);
                    arrayListToListView(arrayList);

                    textBoxName.Text = "";
                    textBoxPrice.Text = "";
                    textBoxDes.Text = "";
                    textBoxAmount.Text = "";
                }
                else
                {
                    myPath = "";
                    MessageBox.Show("Please try again!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }                                 
        }

        private void arrayListToListView(ArrayList myList)
        {          
            listView1.Items.Clear();           
            for(int i = 0; i < myList.Count; i++)
            {
                Products tmpProduct = (Products)myList[i];               
                string[] stats = { tmpProduct.Name, tmpProduct.Price, tmpProduct.Description, tmpProduct.ImagePath, tmpProduct.Amount };
                ListViewItem newItem = new ListViewItem(stats);
                listView1.Items.Add(newItem);                
            }

            UserListView.Items.Clear();
            for (int i = 0; i < myList.Count; i++)
            {                
                Products tmpProductUser = (Products)myList[i];                              
                string[] statsUser = { tmpProductUser.Name, tmpProductUser.Price, tmpProductUser.Description, tmpProductUser.ImagePath, tmpProductUser.Amount };              
                ListViewItem newItemUser = new ListViewItem(statsUser);
                UserListView.Items.Add(newItemUser);
            }           
        }

        private void arrayListToListViewCart(ArrayList myList)
        {
            listviewCart.Items.Clear();
            for (int i = 0; i < myList.Count; i++)
            {
                CartProducts tmpPorductCart = (CartProducts)myList[i];
                string[] statsCart = { tmpPorductCart.NameCart, tmpPorductCart.AmountCart, tmpPorductCart.PerPriceCart, tmpPorductCart.PriceCart };
                ListViewItem newItemCart = new ListViewItem(statsCart);
                listviewCart.Items.Add(newItemCart);
            }
        }

        private void OnlineGrocery_Load(object sender, EventArgs e)
        {                     
            string path;          

            path = Path.Combine(Environment.CurrentDirectory, @"Pictures\", "Water.jpg");
            Products productWater = new Products("Water", "25", "Source of life.", path, "5");
            arrayList.Add(productWater);

            path = Path.Combine(Environment.CurrentDirectory, @"Pictures\", "Milk.jpg");
            Products productMilk = new Products("Milk", "5", "Organic milk.", path, "55");            
            arrayList.Add(productMilk);

            path = Path.Combine(Environment.CurrentDirectory, @"Pictures\", "Bread.jpg");
            Products productBread = new Products("Bread", "1", "Fresh bread.", path, "100");
            arrayList.Add(productBread);

            path = Path.Combine(Environment.CurrentDirectory, @"Pictures\", "Rice.jpg");
            Products productRice = new Products("Rice", "21", "White rice.", path, "42");
            arrayList.Add(productRice);

            path = Path.Combine(Environment.CurrentDirectory, @"Pictures\", "Meat.jpg");
            Products productMeat = new Products("Meat", "90", "Red meat.", path, "14");
            arrayList.Add(productMeat);

            path = Path.Combine(Environment.CurrentDirectory, @"Pictures\", "Eggs.jpg");
            Products productEgg = new Products("Egg", "11", "Farm egg.", path, "13");
            arrayList.Add(productEgg);

            path = Path.Combine(Environment.CurrentDirectory, @"Pictures\", "Cheese.jpg");
            Products productCheese = new Products("Cheese", "45", "All kinds of cheese.", path, "44");
            arrayList.Add(productCheese);

            path = Path.Combine(Environment.CurrentDirectory, @"Pictures\", "Oil.jpg");
            Products productOliveOil = new Products("Olive Oil", "115", "Healty olive oil.", path, "74");
            arrayList.Add(productOliveOil);

            path = Path.Combine(Environment.CurrentDirectory, @"Pictures\", "Pasta.jpg");
            Products productPasta = new Products("Pasta", "4", "Italian pasta.", path, "155");
            arrayList.Add(productPasta);

            path = Path.Combine(Environment.CurrentDirectory, @"Pictures\", "Fish.jpg");
            Products productFish = new Products("Fish", "32", "Fresh and cheap fish.", path, "48");
            arrayList.Add(productFish);

            arrayListToListView(arrayList);
        }

        private void OnlineGrocery_Activated(object sender, EventArgs e)
        {
            if(informationwindow.infoSetGet == true || informationwindow.infoReturnSetGet == true)
            {
                TabControl.SelectedTab = TabControl.TabPages[1];
            }           
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {           
            for (int i = 0; i < listView1.SelectedIndices.Count; i++)
            {
                var temp = listView1.SelectedIndices[i]; 
                selected = temp;
            }
                  
            textBoxName.Text = listView1.Items[selected].SubItems[0].Text;           
            textBoxPrice.Text = listView1.Items[selected].SubItems[1].Text;
            textBoxDes.Text = listView1.Items[selected].SubItems[2].Text;
            textBoxAmount.Text = listView1.Items[selected].SubItems[4].Text;
            
            string images = listView1.Items[selected].SubItems[3].Text;
            pictureBox1.Image = Image.FromFile(images);
        }

        private void updatebtn_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                if (textBoxName.Text == String.Empty || textBoxPrice.Text == String.Empty || textBoxDes.Text == String.Empty || textBoxAmount.Text == String.Empty)
                {
                    MessageBox.Show("Please don't leave textboxes empty!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    Products tmpProducts = new Products("", "", "", "", "");
                    tmpProducts = (Products)arrayList[selected];

                    string myPath;
                    OpenFileDialog openFileDialog = new OpenFileDialog();
                    openFileDialog.Title = "Select Image.";
                    openFileDialog.Filter = ".JPG|*.jpg|PNG|*.png";
                    if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        pictureBox1.Image = new Bitmap(openFileDialog.FileName);
                        myPath = System.IO.Path.GetFullPath(openFileDialog.FileName);

                        tmpProducts.Name = textBoxName.Text;
                        tmpProducts.Price = textBoxPrice.Text;
                        tmpProducts.Description = textBoxDes.Text;
                        tmpProducts.ImagePath = myPath;
                        tmpProducts.Amount = textBoxAmount.Text;

                        arrayList[selected] = tmpProducts;
                        arrayListToListView(arrayList);
                    }
                    else
                    {
                        myPath = "";
                        MessageBox.Show("Please try again!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);                       
                    }                   
                }
            }
            else
            {
                MessageBox.Show("Please select a item!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }                     
        }

        private void delbtn_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                arrayList.RemoveAt(selected);
                arrayListToListView(arrayList);
               
                textBoxName.Text = String.Empty;
                textBoxPrice.Text = String.Empty;
                textBoxDes.Text = String.Empty;
                pictureBox1.Image = null;
                textBoxAmount.Text = String.Empty;
            }
            else
            {
                MessageBox.Show("Please select a item!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }           
        }

        private void textBoxPrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void textBoxAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void cleanbtn_Click(object sender, EventArgs e)
        {           
            textBoxName.Text = String.Empty;
            textBoxPrice.Text = String.Empty;
            textBoxDes.Text = String.Empty;
            pictureBox1.Image = null;
            textBoxAmount.Text = String.Empty;
        }

        public void hideTab()
        {
            TabControl.TabPages.Remove(TabAdmin);           
        }
        
        //User Store
        private void UserListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < UserListView.SelectedIndices.Count; i++)
            {
                var temp = UserListView.SelectedIndices[i];               
                selected = temp;
            }
           
            string imagesUser = UserListView.Items[selected].SubItems[3].Text;
            UserpictureBox.Image = Image.FromFile(imagesUser);

            informationwindow.nameSetGet = UserListView.Items[selected].SubItems[0].Text;
            informationwindow.priceSetGet = UserListView.Items[selected].SubItems[1].Text;
            informationwindow.perPriceSetGet = UserListView.Items[selected].SubItems[1].Text;
            informationwindow.DescSetGet = UserListView.Items[selected].SubItems[2].Text;
            informationwindow.infoImageSetGet = UserListView.Items[selected].SubItems[3].Text;
            informationwindow.AmountSetGet = UserListView.Items[selected].SubItems[4].Text;         
        }

        private void UserpictureBox_Click(object sender, EventArgs e)
        {
            if (UserListView.SelectedItems.Count <= 0)
            {
                MessageBox.Show("Please select a item!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }          
            else
            {
                informationwindow.Show();
                btnLogout.Enabled = false;
                UserListView.Enabled = false;
                UserpictureBox.Enabled = false;
                TabControl.Enabled = false;
            }       
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            this.Hide();
            windowLogin login = new windowLogin();
            login.Show();
        }

        private void TabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (informationwindow.infoSetGet == true)
            {
                int tmpPrice = Int32.Parse(informationwindow.textBoxAmountSetGet) * Int32.Parse(informationwindow.priceSetGet);
                CartProducts myCart = new CartProducts(informationwindow.nameSetGet, informationwindow.textBoxAmountSetGet, informationwindow.perPriceSetGet, tmpPrice.ToString());
                arrayListCart.Add(myCart);
                arrayListToListViewCart(arrayListCart);
                                             
                int tmpStock = Int32.Parse(UserListView.Items[selected].SubItems[4].Text) - Int32.Parse(informationwindow.textBoxAmountSetGet);
                informationwindow.AmountSetGet = tmpStock.ToString();
                UserListView.Items[selected].SubItems[4].Text = tmpStock.ToString();
                listView1.Items[selected].SubItems[4].Text = tmpStock.ToString();

                informationwindow.infoSetGet = false;
                btnLogout.Enabled = true;
                UserListView.Enabled = true;
                UserpictureBox.Enabled = true;
                TabControl.Enabled = true;              
            }
            else if (informationwindow.infoReturnSetGet == true)
            {
                btnLogout.Enabled = true;
                UserListView.Enabled = true;
                UserpictureBox.Enabled = true;
                TabControl.Enabled = true;
                informationwindow.infoReturnSetGet = false;                
            }
        }

        private void btnCartDelete_Click(object sender, EventArgs e)
        {           
            if (listviewCart.SelectedItems.Count > 0)
            {
                int tmpStock = Int32.Parse(UserListView.Items[selected].SubItems[4].Text) + Int32.Parse(listviewCart.Items[selectedCart].SubItems[1].Text);
                UserListView.Items[selected].SubItems[4].Text = tmpStock.ToString();
                informationwindow.AmountSetGet = tmpStock.ToString();

                arrayListCart.RemoveAt(selectedCart);
                arrayListToListViewCart(arrayListCart);
                textBoxUpdate.Text = String.Empty;                          
            }
            else
            {
                MessageBox.Show("Please select a item!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void listviewCart_SelectedIndexChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < listviewCart.SelectedIndices.Count; i++)
            {
                var temp = listviewCart.SelectedIndices[i];
                selectedCart = temp;
            }
            textBoxUpdate.Text = listviewCart.Items[selectedCart].SubItems[1].Text;
        }

        private void btnCartUpdate_Click(object sender, EventArgs e)
        {
            int newIndex = getIndexofProduct(listviewCart.Items[selectedCart].SubItems[0].Text);

            if (listviewCart.SelectedItems.Count > 0)
            {
                if (textBoxUpdate.Text == String.Empty)
                {
                    MessageBox.Show("Please don't leave textboxes empty!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }         
                else if (Int32.Parse(listviewCart.Items[selectedCart].SubItems[1].Text) > Int32.Parse(UserListView.Items[newIndex].SubItems[4].Text))
                {
                    MessageBox.Show("Out of stock!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {                  
                    if (Int32.Parse(listviewCart.Items[selectedCart].SubItems[1].Text) > Int32.Parse(textBoxUpdate.Text))
                    {
                        CartProducts tmpCartProducts = new CartProducts("", "", "", "");
                        tmpCartProducts = (CartProducts)arrayListCart[selectedCart];

                        tmpCartProducts.AmountCart = textBoxUpdate.Text;
                        int tmpPrice = Int32.Parse(tmpCartProducts.AmountCart) * Int32.Parse(listviewCart.Items[selectedCart].SubItems[2].Text);
                        tmpCartProducts.PriceCart = tmpPrice.ToString();                       

                        int addDiff = Int32.Parse(listviewCart.Items[selectedCart].SubItems[1].Text) - Int32.Parse(textBoxUpdate.Text);
                        addDiff = Int32.Parse(UserListView.Items[newIndex].SubItems[4].Text) + addDiff;
                        UserListView.Items[newIndex].SubItems[4].Text = addDiff.ToString();
                        listView1.Items[newIndex].SubItems[4].Text = addDiff.ToString();                        

                        informationwindow.AmountSetGet = listView1.Items[newIndex].SubItems[4].Text;

                        arrayListCart[selectedCart] = tmpCartProducts;
                        arrayListToListViewCart(arrayListCart);
                    }
                    else if (Int32.Parse(listviewCart.Items[selectedCart].SubItems[1].Text) < Int32.Parse(textBoxUpdate.Text))
                    {
                        CartProducts tmpCartProducts = new CartProducts("", "", "", "");
                        tmpCartProducts = (CartProducts)arrayListCart[selectedCart];

                        tmpCartProducts.AmountCart = textBoxUpdate.Text;
                        int tmpPrice = Int32.Parse(tmpCartProducts.AmountCart) * Int32.Parse(listviewCart.Items[selectedCart].SubItems[2].Text);
                        tmpCartProducts.PriceCart = tmpPrice.ToString();
                     
                        int addDiff = Int32.Parse(textBoxUpdate.Text) - Int32.Parse(listviewCart.Items[selectedCart].SubItems[1].Text);
                        addDiff = Int32.Parse(UserListView.Items[newIndex].SubItems[4].Text) - addDiff;
                        UserListView.Items[newIndex].SubItems[4].Text = addDiff.ToString();
                        listView1.Items[newIndex].SubItems[4].Text = addDiff.ToString();

                        informationwindow.AmountSetGet = listView1.Items[newIndex].SubItems[4].Text;

                        arrayListCart[selectedCart] = tmpCartProducts;
                        arrayListToListViewCart(arrayListCart);
                    }
                    else
                    {
                        CartProducts tmpCartProducts = new CartProducts("", "", "", "");
                        tmpCartProducts = (CartProducts)arrayListCart[selectedCart];

                        tmpCartProducts.AmountCart = textBoxUpdate.Text;
                        int tmpPrice = Int32.Parse(tmpCartProducts.AmountCart) * Int32.Parse(listviewCart.Items[selectedCart].SubItems[2].Text);
                        tmpCartProducts.PriceCart = tmpPrice.ToString();

                        arrayListCart[selectedCart] = tmpCartProducts;
                        arrayListToListViewCart(arrayListCart);
                    }                                    
                }
            }
            else
            {
                MessageBox.Show("Please select a item!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnPurchase_Click(object sender, EventArgs e)
        {
            if (arrayListCart.Count > 0)
            {
                while (arrayListCart.Count > 0)
                {
                    for (int i = 0; i < arrayListCart.Count; i++)
                    {                       
                        arrayListCart.RemoveAt(i);
                        arrayListToListViewCart(arrayListCart);                       
                    }
                }
                textBoxUpdate.Text = "";
                MessageBox.Show("Your purchase was successful.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            else
            {              
                MessageBox.Show("You have no items in your shopping cart!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnLogOutCart_Click(object sender, EventArgs e)
        {
            this.Hide();
            windowLogin login = new windowLogin();
            login.Show();
        }

        private int getIndexofProduct(string myProduct)
        {
            int index = 0;
            List<string> productNames = new List<string>(UserListView.Items.Count);

            for (int i = 0; i < UserListView.Items.Count; i++)
            {                
                productNames.Add(UserListView.Items[i].SubItems[0].Text);
            }

            index = productNames.IndexOf(myProduct);
            return index;
        }
    }
}
