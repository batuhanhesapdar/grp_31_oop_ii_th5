﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_Application
{
    class CartProducts
    {
        private string name;
        private string price;
        private string perPrice;
        private string amount;

        public string NameCart
        {
            get { return name; }
            set { name = value; }
        }

        public string PriceCart
        {
            get { return price; }
            set { price = value; }
        }

        public string PerPriceCart
        {
            get { return perPrice; }
            set { perPrice = value; }
        }

        public string AmountCart
        {
            get { return amount; }
            set { amount = value; }
        }

        public CartProducts(string MyName, string MyAmount, string MyPerPrice, string MyPrice)
        {
            name = MyName;
            perPrice = MyPerPrice;
            price = MyPrice;            
            amount = MyAmount;
        }
    }
}
