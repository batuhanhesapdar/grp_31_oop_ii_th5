﻿
namespace Lab_Application
{
    partial class OnlineGrocery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Addbtn = new System.Windows.Forms.Button();
            this.updatebtn = new System.Windows.Forms.Button();
            this.delbtn = new System.Windows.Forms.Button();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.listView1 = new System.Windows.Forms.ListView();
            this.Product = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Price1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Description1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Amount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxPrice = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxDes = new System.Windows.Forms.TextBox();
            this.cleanbtn = new System.Windows.Forms.Button();
            this.TabControl = new System.Windows.Forms.TabControl();
            this.TabStore = new System.Windows.Forms.TabPage();
            this.label9 = new System.Windows.Forms.Label();
            this.btnLogout = new System.Windows.Forms.Button();
            this.UserpictureBox = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.UserListView = new System.Windows.Forms.ListView();
            this.UserProduct = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.UserPrice = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.UserDescription = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.UserImage = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.UserAmount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TabCard = new System.Windows.Forms.TabPage();
            this.btnLogOutCart = new System.Windows.Forms.Button();
            this.textBoxUpdate = new System.Windows.Forms.TextBox();
            this.btnCartUpdate = new System.Windows.Forms.Button();
            this.btnCartDelete = new System.Windows.Forms.Button();
            this.btnPurchase = new System.Windows.Forms.Button();
            this.listviewCart = new System.Windows.Forms.ListView();
            this.CartProduct = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.CartAmount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.CartUnitPrice = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.CartPrice = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TabAdmin = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxAmount = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.TabControl.SuspendLayout();
            this.TabStore.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UserpictureBox)).BeginInit();
            this.TabCard.SuspendLayout();
            this.TabAdmin.SuspendLayout();
            this.SuspendLayout();
            // 
            // Addbtn
            // 
            this.Addbtn.Location = new System.Drawing.Point(13, 196);
            this.Addbtn.Name = "Addbtn";
            this.Addbtn.Size = new System.Drawing.Size(100, 23);
            this.Addbtn.TabIndex = 0;
            this.Addbtn.Text = "Add";
            this.Addbtn.UseVisualStyleBackColor = true;
            this.Addbtn.Click += new System.EventHandler(this.Addbtn_Click);
            // 
            // updatebtn
            // 
            this.updatebtn.Location = new System.Drawing.Point(13, 225);
            this.updatebtn.Name = "updatebtn";
            this.updatebtn.Size = new System.Drawing.Size(100, 23);
            this.updatebtn.TabIndex = 1;
            this.updatebtn.Text = "Update";
            this.updatebtn.UseVisualStyleBackColor = true;
            this.updatebtn.Click += new System.EventHandler(this.updatebtn_Click);
            // 
            // delbtn
            // 
            this.delbtn.Location = new System.Drawing.Point(13, 254);
            this.delbtn.Name = "delbtn";
            this.delbtn.Size = new System.Drawing.Size(100, 23);
            this.delbtn.TabIndex = 2;
            this.delbtn.Text = "Delete";
            this.delbtn.UseVisualStyleBackColor = true;
            this.delbtn.Click += new System.EventHandler(this.delbtn_Click);
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(13, 34);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(100, 20);
            this.textBoxName.TabIndex = 4;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(504, 33);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(175, 138);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(149, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "List of Products";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(501, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Image";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(387, 158);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 9;
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Product,
            this.Price1,
            this.Description1,
            this.columnHeader1,
            this.Amount});
            this.listView1.FullRowSelect = true;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(152, 34);
            this.listView1.MultiSelect = false;
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(346, 272);
            this.listView1.TabIndex = 12;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // Product
            // 
            this.Product.Text = "Product";
            this.Product.Width = 75;
            // 
            // Price1
            // 
            this.Price1.Text = "Price (Lira)";
            this.Price1.Width = 75;
            // 
            // Description1
            // 
            this.Description1.Text = "Description";
            this.Description1.Width = 110;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Width = 0;
            // 
            // Amount
            // 
            this.Amount.Text = "Stock";
            this.Amount.Width = 80;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 17);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Name";
            // 
            // textBoxPrice
            // 
            this.textBoxPrice.Location = new System.Drawing.Point(13, 77);
            this.textBoxPrice.Name = "textBoxPrice";
            this.textBoxPrice.Size = new System.Drawing.Size(100, 20);
            this.textBoxPrice.TabIndex = 14;
            this.textBoxPrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxPrice_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 61);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Price";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 105);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "Description";
            // 
            // textBoxDes
            // 
            this.textBoxDes.Location = new System.Drawing.Point(13, 121);
            this.textBoxDes.Name = "textBoxDes";
            this.textBoxDes.Size = new System.Drawing.Size(100, 20);
            this.textBoxDes.TabIndex = 17;
            // 
            // cleanbtn
            // 
            this.cleanbtn.Location = new System.Drawing.Point(13, 283);
            this.cleanbtn.Name = "cleanbtn";
            this.cleanbtn.Size = new System.Drawing.Size(100, 23);
            this.cleanbtn.TabIndex = 18;
            this.cleanbtn.Text = "Clean";
            this.cleanbtn.UseVisualStyleBackColor = true;
            this.cleanbtn.Click += new System.EventHandler(this.cleanbtn_Click);
            // 
            // TabControl
            // 
            this.TabControl.Controls.Add(this.TabStore);
            this.TabControl.Controls.Add(this.TabCard);
            this.TabControl.Controls.Add(this.TabAdmin);
            this.TabControl.Location = new System.Drawing.Point(12, 12);
            this.TabControl.Name = "TabControl";
            this.TabControl.SelectedIndex = 0;
            this.TabControl.Size = new System.Drawing.Size(693, 338);
            this.TabControl.TabIndex = 19;
            this.TabControl.SelectedIndexChanged += new System.EventHandler(this.TabControl_SelectedIndexChanged);
            // 
            // TabStore
            // 
            this.TabStore.Controls.Add(this.label9);
            this.TabStore.Controls.Add(this.btnLogout);
            this.TabStore.Controls.Add(this.UserpictureBox);
            this.TabStore.Controls.Add(this.label4);
            this.TabStore.Controls.Add(this.UserListView);
            this.TabStore.Location = new System.Drawing.Point(4, 22);
            this.TabStore.Name = "TabStore";
            this.TabStore.Padding = new System.Windows.Forms.Padding(3);
            this.TabStore.Size = new System.Drawing.Size(685, 312);
            this.TabStore.TabIndex = 1;
            this.TabStore.Text = "Store";
            this.TabStore.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(453, 11);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(149, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "Click the picture below to buy.";
            // 
            // btnLogout
            // 
            this.btnLogout.Location = new System.Drawing.Point(528, 283);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(75, 23);
            this.btnLogout.TabIndex = 3;
            this.btnLogout.Text = "Logout";
            this.btnLogout.UseVisualStyleBackColor = true;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // UserpictureBox
            // 
            this.UserpictureBox.Location = new System.Drawing.Point(453, 30);
            this.UserpictureBox.Name = "UserpictureBox";
            this.UserpictureBox.Size = new System.Drawing.Size(226, 247);
            this.UserpictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.UserpictureBox.TabIndex = 2;
            this.UserpictureBox.TabStop = false;
            this.UserpictureBox.Click += new System.EventHandler(this.UserpictureBox_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 11);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "List of Products";
            // 
            // UserListView
            // 
            this.UserListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.UserProduct,
            this.UserPrice,
            this.UserDescription,
            this.UserImage,
            this.UserAmount});
            this.UserListView.FullRowSelect = true;
            this.UserListView.HideSelection = false;
            this.UserListView.Location = new System.Drawing.Point(6, 30);
            this.UserListView.MultiSelect = false;
            this.UserListView.Name = "UserListView";
            this.UserListView.Size = new System.Drawing.Size(441, 276);
            this.UserListView.TabIndex = 0;
            this.UserListView.UseCompatibleStateImageBehavior = false;
            this.UserListView.View = System.Windows.Forms.View.Details;
            this.UserListView.SelectedIndexChanged += new System.EventHandler(this.UserListView_SelectedIndexChanged);
            // 
            // UserProduct
            // 
            this.UserProduct.Text = "Product";
            this.UserProduct.Width = 125;
            // 
            // UserPrice
            // 
            this.UserPrice.Text = "Price (Lira)";
            this.UserPrice.Width = 125;
            // 
            // UserDescription
            // 
            this.UserDescription.Text = "Description";
            this.UserDescription.Width = 125;
            // 
            // UserImage
            // 
            this.UserImage.Width = 0;
            // 
            // UserAmount
            // 
            this.UserAmount.Text = "Stock";
            // 
            // TabCard
            // 
            this.TabCard.Controls.Add(this.btnLogOutCart);
            this.TabCard.Controls.Add(this.textBoxUpdate);
            this.TabCard.Controls.Add(this.btnCartUpdate);
            this.TabCard.Controls.Add(this.btnCartDelete);
            this.TabCard.Controls.Add(this.btnPurchase);
            this.TabCard.Controls.Add(this.listviewCart);
            this.TabCard.Location = new System.Drawing.Point(4, 22);
            this.TabCard.Name = "TabCard";
            this.TabCard.Size = new System.Drawing.Size(685, 312);
            this.TabCard.TabIndex = 2;
            this.TabCard.Text = "Shopping Cart";
            this.TabCard.UseVisualStyleBackColor = true;
            // 
            // btnLogOutCart
            // 
            this.btnLogOutCart.Location = new System.Drawing.Point(61, 239);
            this.btnLogOutCart.Name = "btnLogOutCart";
            this.btnLogOutCart.Size = new System.Drawing.Size(75, 49);
            this.btnLogOutCart.TabIndex = 5;
            this.btnLogOutCart.Text = "Logout";
            this.btnLogOutCart.UseVisualStyleBackColor = true;
            this.btnLogOutCart.Click += new System.EventHandler(this.btnLogOutCart_Click);
            // 
            // textBoxUpdate
            // 
            this.textBoxUpdate.Location = new System.Drawing.Point(346, 239);
            this.textBoxUpdate.Name = "textBoxUpdate";
            this.textBoxUpdate.Size = new System.Drawing.Size(129, 20);
            this.textBoxUpdate.TabIndex = 4;
            // 
            // btnCartUpdate
            // 
            this.btnCartUpdate.Location = new System.Drawing.Point(346, 265);
            this.btnCartUpdate.Name = "btnCartUpdate";
            this.btnCartUpdate.Size = new System.Drawing.Size(129, 23);
            this.btnCartUpdate.TabIndex = 3;
            this.btnCartUpdate.Text = "Update Amount";
            this.btnCartUpdate.UseVisualStyleBackColor = true;
            this.btnCartUpdate.Click += new System.EventHandler(this.btnCartUpdate_Click);
            // 
            // btnCartDelete
            // 
            this.btnCartDelete.Location = new System.Drawing.Point(206, 239);
            this.btnCartDelete.Name = "btnCartDelete";
            this.btnCartDelete.Size = new System.Drawing.Size(75, 49);
            this.btnCartDelete.TabIndex = 2;
            this.btnCartDelete.Text = "Remove";
            this.btnCartDelete.UseVisualStyleBackColor = true;
            this.btnCartDelete.Click += new System.EventHandler(this.btnCartDelete_Click);
            // 
            // btnPurchase
            // 
            this.btnPurchase.Location = new System.Drawing.Point(515, 239);
            this.btnPurchase.Name = "btnPurchase";
            this.btnPurchase.Size = new System.Drawing.Size(129, 49);
            this.btnPurchase.TabIndex = 1;
            this.btnPurchase.Text = "Complete your purchase";
            this.btnPurchase.UseVisualStyleBackColor = true;
            this.btnPurchase.Click += new System.EventHandler(this.btnPurchase_Click);
            // 
            // listviewCart
            // 
            this.listviewCart.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.CartProduct,
            this.CartAmount,
            this.CartUnitPrice,
            this.CartPrice});
            this.listviewCart.FullRowSelect = true;
            this.listviewCart.HideSelection = false;
            this.listviewCart.Location = new System.Drawing.Point(3, 3);
            this.listviewCart.MultiSelect = false;
            this.listviewCart.Name = "listviewCart";
            this.listviewCart.Size = new System.Drawing.Size(679, 217);
            this.listviewCart.TabIndex = 0;
            this.listviewCart.UseCompatibleStateImageBehavior = false;
            this.listviewCart.View = System.Windows.Forms.View.Details;
            this.listviewCart.SelectedIndexChanged += new System.EventHandler(this.listviewCart_SelectedIndexChanged);
            // 
            // CartProduct
            // 
            this.CartProduct.Text = "Product";
            this.CartProduct.Width = 140;
            // 
            // CartAmount
            // 
            this.CartAmount.Text = "Amount";
            this.CartAmount.Width = 140;
            // 
            // CartUnitPrice
            // 
            this.CartUnitPrice.Text = "Unit Price (Lira)";
            this.CartUnitPrice.Width = 140;
            // 
            // CartPrice
            // 
            this.CartPrice.Text = "Products Total Price (Lira)";
            this.CartPrice.Width = 140;
            // 
            // TabAdmin
            // 
            this.TabAdmin.Controls.Add(this.label5);
            this.TabAdmin.Controls.Add(this.textBoxAmount);
            this.TabAdmin.Controls.Add(this.listView1);
            this.TabAdmin.Controls.Add(this.cleanbtn);
            this.TabAdmin.Controls.Add(this.Addbtn);
            this.TabAdmin.Controls.Add(this.textBoxDes);
            this.TabAdmin.Controls.Add(this.updatebtn);
            this.TabAdmin.Controls.Add(this.label8);
            this.TabAdmin.Controls.Add(this.delbtn);
            this.TabAdmin.Controls.Add(this.label7);
            this.TabAdmin.Controls.Add(this.textBoxName);
            this.TabAdmin.Controls.Add(this.textBoxPrice);
            this.TabAdmin.Controls.Add(this.pictureBox1);
            this.TabAdmin.Controls.Add(this.label6);
            this.TabAdmin.Controls.Add(this.label1);
            this.TabAdmin.Controls.Add(this.label2);
            this.TabAdmin.Controls.Add(this.label3);
            this.TabAdmin.Location = new System.Drawing.Point(4, 22);
            this.TabAdmin.Name = "TabAdmin";
            this.TabAdmin.Padding = new System.Windows.Forms.Padding(3);
            this.TabAdmin.Size = new System.Drawing.Size(685, 312);
            this.TabAdmin.TabIndex = 0;
            this.TabAdmin.Text = "Admin Panel";
            this.TabAdmin.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 148);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "Amount";
            // 
            // textBoxAmount
            // 
            this.textBoxAmount.Location = new System.Drawing.Point(13, 164);
            this.textBoxAmount.Name = "textBoxAmount";
            this.textBoxAmount.Size = new System.Drawing.Size(100, 20);
            this.textBoxAmount.TabIndex = 19;
            this.textBoxAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxAmount_KeyPress);
            // 
            // OnlineGrocery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(717, 380);
            this.Controls.Add(this.TabControl);
            this.Name = "OnlineGrocery";
            this.Text = "OnlineGrocery";
            this.Activated += new System.EventHandler(this.OnlineGrocery_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnlineGrocery_FormClosing);
            this.Load += new System.EventHandler(this.OnlineGrocery_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.TabControl.ResumeLayout(false);
            this.TabStore.ResumeLayout(false);
            this.TabStore.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UserpictureBox)).EndInit();
            this.TabCard.ResumeLayout(false);
            this.TabCard.PerformLayout();
            this.TabAdmin.ResumeLayout(false);
            this.TabAdmin.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Addbtn;
        private System.Windows.Forms.Button updatebtn;
        private System.Windows.Forms.Button delbtn;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader Product;
        private System.Windows.Forms.ColumnHeader Price1;
        private System.Windows.Forms.ColumnHeader Description1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxPrice;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxDes;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.Button cleanbtn;
        private System.Windows.Forms.TabPage TabAdmin;
        private System.Windows.Forms.TabPage TabStore;
        private System.Windows.Forms.TabPage TabCard;
        private System.Windows.Forms.TabControl TabControl;
        private System.Windows.Forms.PictureBox UserpictureBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListView UserListView;
        private System.Windows.Forms.ColumnHeader UserProduct;
        private System.Windows.Forms.ColumnHeader UserPrice;
        private System.Windows.Forms.ColumnHeader UserDescription;
        private System.Windows.Forms.ColumnHeader UserImage;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxAmount;
        private System.Windows.Forms.ColumnHeader Amount;
        private System.Windows.Forms.ColumnHeader UserAmount;
        private System.Windows.Forms.ListView listviewCart;
        private System.Windows.Forms.Button btnPurchase;
        private System.Windows.Forms.ColumnHeader CartProduct;
        private System.Windows.Forms.ColumnHeader CartPrice;
        private System.Windows.Forms.ColumnHeader CartAmount;
        private System.Windows.Forms.Button btnCartDelete;
        private System.Windows.Forms.Button btnCartUpdate;
        private System.Windows.Forms.TextBox textBoxUpdate;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ColumnHeader CartUnitPrice;
        private System.Windows.Forms.Button btnLogOutCart;
    }
}