﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_Application
{
    public class Admin
    {
        private static Admin instance;
        private Dictionary<string, string> AdminTable;
        private Admin()
        {
            AdminTable = new Dictionary<string, string>();
        }
        public static Admin GetInstance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Admin();
                }
                return instance;
            }
        }
        /// <summary>
        /// Take name and password and add it to user table
        /// </summary>
        /// <param name="name">Username key</param>
        /// <param name="password">Password(Value) of the username</param>
        /// <returns>A boolean variable that prevents duplicates</returns>
        public bool AddUser(string name, string password)
        {
            if (AdminTable.ContainsKey(name)) return false; // Avoid duplicates
            AdminTable.Add(name, password);
            return true;
        }
        /// <summary>
        /// Take name and password and check the user table
        /// </summary>
        /// <param name="name">Username key</param>
        /// <param name="password">Password(Value) of the username</param>
        /// <returns>A boolean variable that is the result of the checking process</returns>
        public bool CheckForLogin(string name, string password)
        {
            return AdminTable.ContainsKey(name) && AdminTable[name] == password;          
        }
    }
}
