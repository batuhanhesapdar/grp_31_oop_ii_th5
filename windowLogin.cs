﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;

namespace Lab_Application
{
    public partial class windowLogin : Form
    {
        User userList = User.GetInstance;
        Admin adminList = Admin.GetInstance;      
        
        public windowLogin()
        {
            InitializeComponent();
            // To hide passwords
            txtPassword.PasswordChar = '●';
            userList.AddUser("Omer", Encrypt.MD5Hash("123456"));
            adminList.AddUser("admin", Encrypt.MD5Hash("123456"));
        }

        private async void btnLogin_Click(object sender, EventArgs e)
        {
            string password = Encrypt.MD5Hash(txtPassword.Text);
            if (adminList.CheckForLogin(txtUserID.Text, password))
            {
                btnLogin.Enabled = false; // Avoid from multiple windows
                btnSignup.Enabled = false;
                lblSuccess.ForeColor = Color.Green;
                lblSuccess.Text = "Login Successful! Loading...";
                await Task.Delay(3000);
                this.Hide();
                OnlineGrocery adminStore = new OnlineGrocery();               
                adminStore.Show();            
            }
            else if (userList.CheckForLogin(txtUserID.Text, password))
            {
                btnLogin.Enabled = false; // Avoid from multiple windows
                btnSignup.Enabled = false;
                lblSuccess.ForeColor = Color.Green;
                lblSuccess.Text = "Login Successful! Loading...";
                await Task.Delay(3000);
                this.Hide();
                OnlineGrocery adminStore = new OnlineGrocery();
                adminStore.hideTab();
                adminStore.Show();             
            }
            else
            {
                lblSuccess.ForeColor = Color.Red;
                lblSuccess.Text = "Wrong username or password, try again!";
            }
        }

        private async void btnSignup_Click(object sender, EventArgs e)
        {
            await Task.Delay(1000);
            this.Hide();
            windowSignup signup = new windowSignup();
            signup.Show();
        }

        private void windowLogin_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
