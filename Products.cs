﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_Application
{
    class Products
    {
        private string name;
        private string price;
        private string description;
        private string imagePath;
        private string amount;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Price
        {
            get { return price; }
            set { price = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public string ImagePath
        {
            get { return imagePath; }
            set { imagePath = value; }
        }

        public string Amount
        {
            get { return amount; }
            set { amount = value; }
        }

        public Products(string MyName, string MyPrice, string MyDescription, string MyImage, string MyAmount)
        {
            name = MyName;
            price = MyPrice;
            description = MyDescription;
            imagePath = MyImage;
            amount = MyAmount;
        }       
    }
}
