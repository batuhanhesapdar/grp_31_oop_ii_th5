﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_Application
{
    /// <summary>
    /// User adding and check with singleton pattern
    /// </summary>
    public class User
    {
        private static User instance;
        private Dictionary<string, string> UserTable;
        private User()
        {
            UserTable = new Dictionary<string, string>();
        }
        public static User GetInstance
        {
            get
            {
                if (instance == null)
                {
                    instance = new User();
                }
                return instance;
            }
        }
        /// <summary>
        /// Take name and password and add it to user table
        /// </summary>
        /// <param name="name">Username key</param>
        /// <param name="password">Password(Value) of the username</param>
        /// <returns>A boolean variable that prevents duplicates</returns>
        public bool AddUser(string name, string password)
        {
            if (UserTable.ContainsKey(name)) return false; // Avoid duplicates
            UserTable.Add(name, password);
            return true;
        }
        /// <summary>
        /// Take name and password and check the user table
        /// </summary>
        /// <param name="name">Username key</param>
        /// <param name="password">Password(Value) of the username</param>
        /// <returns>A boolean variable that is the result of the checking process</returns>
        public bool CheckForLogin(string name, string password)
        {
            return UserTable.ContainsKey(name) && UserTable[name] == password;
        }       
    }
}
