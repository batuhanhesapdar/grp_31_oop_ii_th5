﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab_Application
{
    public partial class InformationWindow : Form
    {
        string name;
        string Desc;
        string infoImage;
        string price;
        string perPrice;
        string amount;
        string amountTextBox;
        bool infoReturn = false;
        bool info = false;

        public InformationWindow()
        {
            InitializeComponent();
            this.ControlBox = false;
        }      

        private void InformationWindow_Activated(object sender, EventArgs e)
        {
            BigImage.Image = Image.FromFile(infoImageSetGet);
            descLabel.Text = DescSetGet;
            labelPrice.Text = priceSetGet + " Lira";
            labelAmount.Text = AmountSetGet;
        }
      
        public string DescSetGet
        {
            get { return Desc; }
            set { Desc = value; }
        }

        public string infoImageSetGet
        {
            get { return infoImage; }
            set { infoImage = value; }
        }

        public string priceSetGet
        {
            get { return price; }
            set { price = value; }
        }

        public string perPriceSetGet
        {
            get { return perPrice; }
            set { perPrice = value; }
        }

        public string AmountSetGet
        {
            get { return amount; }
            set { amount = value; }
        }

        public bool infoSetGet
        {
            get { return info; }
            set { info = value; }
        }       

        public string textBoxAmountSetGet
        {
            get { return amountTextBox; }
            set { amountTextBox = value; }
        }

        public string nameSetGet
        {
            get { return name; }
            set { name = value; }
        }

        public bool infoReturnSetGet
        {
            get { return infoReturn; }
            set { infoReturn = value; }
        }

        private void btnAddCart_Click(object sender, EventArgs e)
        {
            if (textBoxAmount.Text == String.Empty)
            {
                info = false;
                MessageBox.Show("Please don't leave amount empty!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);              
            }
            else if (Int32.Parse(textBoxAmount.Text) > Int32.Parse(AmountSetGet))
            {
                info = false;
                MessageBox.Show("You can't go above our stock!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                amountTextBox = textBoxAmount.Text;
                textBoxAmount.Text = String.Empty;             
                info = true;
               
                this.Hide();
                this.Refresh();                         
            }
        }

        private void textBoxAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void btnReturn_Click(object sender, EventArgs e)
        {
            infoReturn = true;
            this.Hide();
            this.Refresh();            
        }
    }
}
