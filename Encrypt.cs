﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace Lab_Application
{
    public static class Encrypt
    {
        public static string MD5Hash(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();

            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));

            byte[] result = md5.Hash;

            StringBuilder str = new StringBuilder();

            for (int i = 0; i < result.Length; i++)
            {
                str.Append(result[i].ToString("x2"));
            }

            return str.ToString();
        }

        public static string CeaserChipher(string Text, int RotValue, string Alphabet)
        {
            List<char> alphabet = stringToCharList(Alphabet);
            string encrypted = "";

            foreach (char item in Text)
            {
                encrypted += alphabet[(alphabet.IndexOf(item) + alphabet.Count() + RotValue) % alphabet.Count()];
            }
            return encrypted;
        }

        public static string VigenereChipher(string Text, string KeyValue, string Alphabet)
        {
            List<char> alphabet = stringToCharList(Alphabet);
            string encrypted = "";
            int i = 0;
            foreach (char item in Text)
            {
                int KeyIndex = alphabet.IndexOf(alphabet[alphabet.IndexOf(KeyValue[i % (KeyValue.Length)])]);
                KeyIndex++;

                encrypted += alphabet[(alphabet.IndexOf(item) + alphabet.Count() + KeyIndex) % alphabet.Count()];
                i++;
            }
            return encrypted;
        }

        private static List<char> stringToCharList(string input)
        {
            List<char> list = new List<char>();
            // Eliminate duplicate characters
            for (int i = 0; i < input.Length; i++)
            {
                if (!list.Contains(input[i]))
                {
                    list.Add(input[i]);
                }
            }
            return list;
        }
    }
}
